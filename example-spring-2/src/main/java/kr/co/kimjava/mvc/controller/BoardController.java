package kr.co.kimjava.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kr.co.kimjava.mvc.domain.Board;
import kr.co.kimjava.mvc.service.BoardService;

@RestController
@RequestMapping("/board")
@CrossOrigin("http://localhost:8081/")
public class BoardController {
	
	@Autowired
	private BoardService boardservice;
	
	
	@GetMapping
	public List<Board> getList(){
		return boardservice.getList();
	}
	@GetMapping("/{boardSeq}")
	public Board get(@PathVariable int boardSeq) {
		return boardservice.get(boardSeq);
	}
	@GetMapping("/save")
	public int save(Board parameter) {
		boardservice.save(parameter);
		return parameter.getBoardSeq();
		
	}
	@GetMapping("/delete/{boardSeq}")
	public boolean delete(@PathVariable int boardSeq) {
		Board board =boardservice.get(boardSeq);
		if(board == null) {
			return false;
		}
		boardservice.delete(boardSeq);
		return true;
	}
	
	
	
}
