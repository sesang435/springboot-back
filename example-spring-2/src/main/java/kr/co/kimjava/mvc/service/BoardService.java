package kr.co.kimjava.mvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import kr.co.kimjava.mvc.domain.Board;
import kr.co.kimjava.mvc.repository.BoardRepository;

@Service
public class BoardService {
	
	@Autowired
	private BoardRepository repository;
	
	public List<Board> getList(){
		return repository.getList();
	}
	
	public Board get(int boardSeq) {
		return repository.get(boardSeq);
	}
	
	public int save(Board board) {
		repository.save(board);
		return board.getBoardSeq();
		
	}
	public void delete(int boardSeq) {
		repository.delete(boardSeq);
		
	}
	
	
}
